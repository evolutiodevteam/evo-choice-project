import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  IPxChoiceGetAvailAppointmentRes,
  IPxChoiceGetAvailAppointmentReq,
  IGetPracticesRes,
} from '../models/IPxChoiceGetAvailAppointment.interface';
import { environment } from 'src/environments/environment';
import {
  IPxChoiceReqAppointmentReq,
  IPxChoiceReqAppointmentRes,
} from '../models/IPxChoiceReqAppointment.interface';
import {
  IPxChoiceConfAppointmentReq,
  IPxChoiceConfAppointmentRes,
} from '../models/IPxChoiceConfAppointment.interface';
import { IPxChoicePractice } from '../models/IPxChoiceLogin.interface';

@Injectable({
  providedIn: 'root',
})
export class AppointmentService {
  getChoicePractices(): Promise<IGetPracticesRes> {
    return this._http
      .post<IGetPracticesRes>(
        `${this.URL}/pxChoiceGetPractices`,
{},
        {
          headers: {
            Authorization: `Bearer ${environment.user.jwt}`,
          },
        }
      )
      .toPromise(); 
  }
 
  
  private readonly URL: string = environment.evonnectSSSUrl;

  constructor(private _http: HttpClient) {}

  selectPractice(siteCode: string): Promise<IPxChoiceGetAvailAppointmentRes> {
    return this._http
      .post<IPxChoiceGetAvailAppointmentRes>(
        `${this.URL}/pxChoiceSelectedPractice`,
        { siteCode },
        {
          headers: {
            Authorization: `Bearer ${environment.user.jwt}`,
          },
        }
      )
      .toPromise();
  }

  getAvailableAppointments(
    req: IPxChoiceGetAvailAppointmentReq
  ): Promise<IPxChoiceGetAvailAppointmentRes> {
    return this._http
      .post<IPxChoiceGetAvailAppointmentRes>(
        `${this.URL}/pxChoiceGetAvailAppointments`,
        req,
        {
          headers: {
            Authorization: `Bearer ${environment.user.jwt}`,
          },
        }
      )
      .toPromise();
  }

  requestAppointment(
    req: IPxChoiceReqAppointmentReq
  ): Promise<IPxChoiceReqAppointmentRes> {
    return this._http
      .post<IPxChoiceReqAppointmentRes>(
        `${this.URL}/pxChoiceReqAppointment`,
        req,
        {
          headers: {
            Authorization: `Bearer ${environment.user.jwt}`,
          },
        }
      )
      .toPromise();
  }

  confirmAppointment(
    req: IPxChoiceConfAppointmentReq
  ): Promise<IPxChoiceConfAppointmentRes> {
    return this._http
      .post<IPxChoiceConfAppointmentRes>(
        `${this.URL}/pxChoiceConfAppointment`,
        req,
        {
          headers: {
            Authorization: `Bearer ${environment.user.jwt}`,
          },
        }
      )
      .toPromise();
  }
}
