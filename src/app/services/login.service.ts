import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IPxChoiceLoginRes } from '../models/IPxChoiceLogin.interface';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  private readonly URL: string = environment.evonnectSSSUrl;
  constructor(private _http: HttpClient) { }

  loginAsPatient(
    dob: string,
    pin: string,
    mobile: string,
    email: string
  ): Promise<IPxChoiceLoginRes> {
    return this._http
      .post<IPxChoiceLoginRes>(`${this.URL}/pxChoiceLogin`, {
        pxDOB: dob,
        pxPIN: pin,
        pxMobile: mobile || '',
        pxEmail: email || '',
      })
      .toPromise();
  }

  setUser({ jwt, patientName, welcomeMessage, choicePractices, userId }): void {
    environment.user = {
      jwt,
      patientName,
      welcomeMessage,
      choicePractices,
      userId,
    };
  }
  setStatus(statusCode: string) {
environment.statusCode=statusCode;
}
  setIsRecall(num:string) {
    environment.isRecall = true;
    environment.helpPhoneNumber = num;
  }

  setErrorMessage(errorMessage: string) {
    environment.errorMessage = errorMessage;
  }

  clearUser(): void {
    environment.user = {
      jwt: '',
    };
  }

  browserCheck() {

  }

  /**
 * Method to detect if the browser is a legacy edge browser
 * See https://stackoverflow.com/questions/31721250/how-to-target-windows-10-edge-browser-with-javascript
 */
  isLegacyEdgeBrowser(): boolean {
    return window.navigator.userAgent.indexOf('Edge/') > -1;
  }
  /**
   * Method to detect if the browser is internet explorer
   * See https://stackoverflow.com/questions/31721250/how-to-target-windows-10-edge-browser-with-javascript
   */
  isIEBrowser(): boolean {
    return (
      window.navigator.userAgent.indexOf('MSIE ') > -1 ||
      window.navigator.userAgent.indexOf('Trident') > -1
    );
  }
}
