import { Injectable } from '@angular/core';
import { CanActivate, UrlTree } from '@angular/router';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService implements CanActivate {
  constructor() {}
  canActivate():
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {
    if (environment.user.jwt != '') {
      return true;
    } else {
      return false;
    }
  }
}

@Injectable({
  providedIn: 'root',
})
export class NegateAuthGuardService implements CanActivate {
  constructor(private authGuard: AuthGuardService) {}
  canActivate():
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {
    return !this.authGuard.canActivate();
  }
}

@Injectable({
  providedIn: 'root',
})
export class DoneAuthGuard implements CanActivate {
  constructor() {}
  canActivate():
    | boolean
    | UrlTree
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree> {
    if (environment.user.jwt == '' && environment.user.welcomeMessage) {
      return true;
    } else {
      return false;
    }
  }
}
