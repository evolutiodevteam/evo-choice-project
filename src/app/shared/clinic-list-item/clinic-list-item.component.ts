import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { IPxChoicePractice } from 'src/app/models/IPxChoiceLogin.interface';
import { faChevronCircleDown } from "@fortawesome/free-solid-svg-icons";
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-clinic-list-item',
  templateUrl: './clinic-list-item.component.html',
  styleUrls: ['./clinic-list-item.component.scss'],
  animations: [
    trigger('openClose', [
      state('open', style({
        height: '*',
        overflow:'hidden',
      })),
      state('closed', style({
        height: '*',
      })),
      transition('open => closed', [
        animate('0.1s')
      ]),
      transition('closed => open', [
        animate('0.1s')
      ]),
    ]),
  ],
})
export class ClinicListItemComponent implements OnInit {
  @Input() site: IPxChoicePractice;

  @Output() siteCode: EventEmitter<IPxChoicePractice> = new EventEmitter<IPxChoicePractice>();
  @Output() collapse: EventEmitter<any> = new EventEmitter<Event>();

  @ViewChild('item') item: ElementRef;

  faChevronCircleDown = faChevronCircleDown;

  constructor() { }

  ngOnInit(): void { }

  select() {
    if (!this.site.expanded) {
      this.collapse.emit(null);
      setTimeout(() => {
        this.site.expanded = true;
        this.item.nativeElement.scrollIntoView({ behavior: "smooth", block: "end" })
      }, 100);
    } else this.site.expanded = false;

  }

  confirm() {
    this.siteCode.emit(this.site)
  }
}
