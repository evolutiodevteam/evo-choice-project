import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlotListItemComponent } from './slot-list-item.component';

describe('SlotListItemComponent', () => {
  let component: SlotListItemComponent;
  let fixture: ComponentFixture<SlotListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlotListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlotListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
