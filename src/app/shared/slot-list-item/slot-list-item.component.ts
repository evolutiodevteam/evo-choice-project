import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ISlot } from 'src/app/models/IPxChoiceGetAvailAppointment.interface';

@Component({
  selector: 'app-slot-list-item',
  templateUrl: './slot-list-item.component.html',
  styleUrls: ['./slot-list-item.component.scss']
})
export class SlotListItemComponent implements OnInit {
  @Input() slot: ISlot;
  @Output() select: EventEmitter<ISlot> = new EventEmitter<ISlot>();

  constructor() { }

  ngOnInit(): void {

  }

  selectSlot() {
    if (!this.slot.selected) {
      this.select.emit(this.slot);
      this.slot.selected = true;
    }
  }
}
