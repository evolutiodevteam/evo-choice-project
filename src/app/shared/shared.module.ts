import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClinicListItemComponent } from './clinic-list-item/clinic-list-item.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DateListItemComponent } from './date-list-item/date-list-item.component';
import { SlotListItemComponent } from './slot-list-item/slot-list-item.component';



@NgModule({
  declarations: [ClinicListItemComponent, DateListItemComponent, SlotListItemComponent],
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  exports:[ClinicListItemComponent, DateListItemComponent,SlotListItemComponent]
})
export class SharedModule { }
