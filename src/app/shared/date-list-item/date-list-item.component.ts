import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { IDay } from 'src/app/models/IPxChoiceGetAvailAppointment.interface';

@Component({
  selector: 'app-date-list-item',
  templateUrl: './date-list-item.component.html',
  styleUrls: ['./date-list-item.component.scss']
})
export class DateListItemComponent implements OnInit {
  @Input() day: IDay;
  @Input() index: number;
  @Output() select: EventEmitter<IDay> = new EventEmitter<IDay>();
  @ViewChild('item') item: ElementRef;

  constructor() { }

  ngOnInit(): void { }

  selectDay(goingToNextAvailable: boolean = false) {
    if (!this.day.selected || goingToNextAvailable) {
      this.select.emit(this.day);

      setTimeout(() => {
        this.day.selected = true;
      }, 100);
    }
  }
}
