import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
  IConfirmationObject,
  IPxChoiceConfAppointmentRes,
  IPxChoiceConfAppointmentReq,
} from 'src/app/models/IPxChoiceConfAppointment.interface';
import { AppointmentService } from 'src/app/services/appointment.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss'],
})
export class ConfirmComponent implements OnInit {
  @Input() confirmationObject: IConfirmationObject;
  @Output() notConfirmed: EventEmitter<boolean> = new EventEmitter<boolean>();
  message: string;
  showConfirmation: boolean= false;
  private readonly TIMEOUT: number = 30 * 1000;
  loading: boolean;

  constructor(
    private _appointmentService: AppointmentService,
    private _toastr: ToastrService,
    private _router: Router
  ) {}

  ngOnInit(): void {
    
  }

  async confirm(response: boolean): Promise<void> {
    if (!response) this.notConfirmed.emit(false);
    else {
    this.loading=true;

      const request: IPxChoiceConfAppointmentReq = {
        ...this.confirmationObject,
        refNum:environment.refNum,
      };
      
      try {
        const response: IPxChoiceConfAppointmentRes = await this._appointmentService.confirmAppointment(
          request
        );
        
        if (response.success === 1) {
          environment.user = {
            welcomeMessage: response.messageText,
            jwt: '',
          };
          this.showConfirmation = true;
          this.message = environment.user.welcomeMessage;
          setTimeout(() => {
            environment.user = { jwt: '' };
            this._router.navigate(['auth']);
          }, this.TIMEOUT);
        } else {
          this._toastr.error(response.messageText);
          this.notConfirmed.emit(false);
        }
      } catch (error) {
        // console.log(error);
      }finally{
        this.loading=false;
      }
    }

  }
}
