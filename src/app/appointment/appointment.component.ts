import { Component, OnInit, ViewChild } from '@angular/core';
import Stepper from 'bs-stepper';
import { IConfirmationObject } from '../models/IPxChoiceConfAppointment.interface';
import { ISite } from '../models/ISite.interface';
import { environment } from 'src/environments/environment';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { AppointmentService } from '../services/appointment.service';
import { PickClinicComponent } from './pick-clinic/pick-clinic.component';
import { PickAppointmentComponent } from './pick-appointment/pick-appointment.component';
@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.scss'],
})
export class AppointmentComponent implements OnInit {
  @ViewChild('pickClinic') pickClinic: PickClinicComponent;
  @ViewChild('pickAppointment') pickAppointment: PickAppointmentComponent;
  stepper: Stepper;
  siteCode: string = '';
  keyInfo: string[] = [];
  stepText: string = '';
  confirmationObject: IConfirmationObject;
  name: string = '';
  backIcon = faArrowLeft;
  welcomeMessage: string;
  helpPhoneNumber: string;
  showMessageForOKF: boolean;
  hideStepper: boolean;
  appointmentMessage: any;
  constructor(private _sss: AppointmentService) { }

  ngOnInit(): void {
    if (environment.helpPhoneNumber !== '') this.helpPhoneNumber = environment.helpPhoneNumber; 
    else this.helpPhoneNumber = '0203 780 7860';

    this.stepper = new Stepper(document.querySelector('#stepper1'), {
      linear: true,
      animation: true,
    });


    if (environment.isRecall) {
      this.stepText = 'Login';
      const site: ISite = this.getSiteFromUser();
      this.setSiteCode(site);
      if(environment.statusCode==='OKF') this.showMessageForOKF=true;
    } else {
      this.stepText = "Clinic"
    }

    this.welcomeMessage = environment.user.welcomeMessage;
  }

  getSiteFromUser(): ISite {
    const siteCode = environment.user.choicePractices[0].siteCode;
    const siteName = environment.user.choicePractices[0].practiceName;
    const doesOnline = environment.user.choicePractices[0].doesOnline;

    return {
      siteCode,
      doesOnline,
      siteName,
    }
  }

  setSiteCode(event: ISite): void {
    if (event.doesOnline === 1) {
      this.siteCode = event.siteCode;
      this.name = event.siteName;
      this.keyInfo = event.keyInfo;
      this.stepper.next();
    } else {
      this.confirmationObject = {
        siteCode: event.siteCode,
        messageText: `Please confirm you would like to be seen at ${event.siteName}. The practice will contact you to arrange an appointment.`,
        keyInfo: event.keyInfo,
      };
      this.stepper.to(3);
    }

  }

  setConfirmationObject($event: IConfirmationObject): void {
    this.confirmationObject = $event;
    this.confirmationObject.keyInfo = this.keyInfo;
    this.stepper.next();
  }

  async goBack(): Promise<void> {
   await this._sss.getChoicePractices().then(res =>{
      environment.user.choicePractices = res.choicePractices;
      this.pickClinic.sites = res.choicePractices;
      this.pickAppointment.deselectAllItems();
      this.pickAppointment.getAvailAppointments();
    });

    if (environment.isRecall) this.stepper.to(2); else this.stepper.to(1);
  }
 

  setAppointmentMessage(message){
    this.appointmentMessage = message;
  }
  noDatesEvent(){
    this.hideStepper=true;
  }
}