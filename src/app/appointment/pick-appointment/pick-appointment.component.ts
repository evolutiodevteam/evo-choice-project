import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  OnInit,
  ViewChild,
  ElementRef,
  ViewChildren,
  QueryList,
} from '@angular/core';
import { AppointmentService } from 'src/app/services/appointment.service';
import {
  IPxChoiceGetAvailAppointmentRes,
  IDay,
  ISlot,
} from 'src/app/models/IPxChoiceGetAvailAppointment.interface';
import {
  IPxChoiceReqAppointmentRes,
  IPxChoiceReqAppointmentReq,
} from 'src/app/models/IPxChoiceReqAppointment.interface';
import { ToastrService } from 'ngx-toastr';
import { IConfirmationObject } from 'src/app/models/IPxChoiceConfAppointment.interface';
import Stepper from 'bs-stepper';
import { environment } from 'src/environments/environment';
import { DateListItemComponent } from 'src/app/shared/date-list-item/date-list-item.component';

@Component({
  selector: 'app-pick-appointment',
  templateUrl: './pick-appointment.component.html',
  styleUrls: ['./pick-appointment.component.scss'],
})
export class PickAppointmentComponent implements OnChanges, OnInit {
  @ViewChildren('dateListItem') dateListItems: QueryList<DateListItemComponent>;
  @Input() siteCode: string = '';
  @Input() stepper: Stepper;
  @Output() confirmationObject: EventEmitter<IConfirmationObject> = new EventEmitter<IConfirmationObject>();
  @ViewChild('confirm') confirm: ElementRef;
  @Output() goBack: EventEmitter<any> = new EventEmitter<any>();
  @Output() appointmentMessage: EventEmitter<any> = new EventEmitter<any>();
  @Output() noDatesEvent: EventEmitter<any> = new EventEmitter<any>();
  days: IDay[] = [];
  // startDate: number;
  // winWidth: number;
  // winHeight: number;
  selectedDate: IDay;

  // selected: number = -1;
  // selectedTime: string = '';
  // selectedDuration = '';
  // selectedClinic = '';
  // slotsForSelectedDay: ISlot[] = [];
  // chosenAppointment: string = '';
  // selectionMade: boolean = false;
  // prevIndex: number;
  // datesToShow: number = 14;
  // showInitialCard: boolean = true;
  hideBackToClinicButton: boolean = false;
  selectedSlot: ISlot;
  loading: boolean;
  noDates: boolean = false;
  message: string;
  
  constructor(
    private _appointmentService: AppointmentService,
    private _toastrService: ToastrService
  ) { }

  ngOnInit(): void {
    // this.winWidth = window.innerWidth;
    // this.winHeight = window.innerHeight;

    if (environment.isRecall) this.hideBackToClinicButton = true;

  }

  async ngOnChanges(): Promise<void> {
    this.getAvailAppointments();
    
  }

  async getAvailAppointments() {  
    this.loading =true;
    this.days = null;
    try {
      if (this.siteCode != '') {
        const res: IPxChoiceGetAvailAppointmentRes = await this._appointmentService.selectPractice(
          this.siteCode
        );

        
this.appointmentMessage.emit(res?.Message);

        if(res.Successful===1){
          this.days = res.Days;
          this.deselectAllItems();
        }else if(res.Successful===-1){
          this.noDates=true;
          this.noDatesEvent.emit();

          environment.errorMessage=res.Message;
        }

        
      }
    } catch (error) {
      // console.error(error);
    }finally{
      this.loading = false;
      
    }
  }


  /**
   * When the Button has been clicked, make the booking request using the selected clinic and appointment slot.
   * If successful go to step 3 Confirm
   * If unsuccessful go to step 1 Clinic
   */
  async selectAppointment(): Promise<void> {
    this.loading = true;
    try {
      // get the appointment and clinic details
      let month = this.selectedDate.month;
      let day = this.selectedDate.day;
      let slotTime = this.selectedSlot.slotTime;
      let duration = this.selectedSlot.duration.toString();
      let clinicName = this.selectedSlot.clinicName;

      //appointment request
      const request: IPxChoiceReqAppointmentReq = {
        siteCode: this.siteCode,
        month,
        day,
        slotTime,
        duration,
        clinicName,
      };

      // make appointment request and wait for response
      const response: IPxChoiceReqAppointmentRes = await this._appointmentService.requestAppointment(
        request
      );

      // if successfully booked
      if (response.success == 1) {
        this.selectedSlot = null;
        this.selectedDate = null;
        this.getAvailAppointments();
        this.confirmationObject.emit({
          ...request,
          messageText: response.messageText,
          pbId: response.pbId,
        });

      }
      // booking unsuccessful - slot has been taken after selecting but before request
      else if (response.success == -1) {
        this._toastrService.error(
          response.messageText,
          'Error',
          {
            closeButton: true,
            disableTimeOut: true,
          }
        );
        this.selectedSlot = null;
        this.selectedDate = null;
        this.getAvailAppointments();
      } else {
        this._toastrService.error(response.messageText);
      }
    }
    // error with request
    catch (error) {
      // console.error(error);
    }
    this.loading = false;
  }

  goToNextAvail() {
    //find the date for the selected date
    const nextAvailSplit = this.selectedDate?.nextAvail?.split(' ');
    const dow = nextAvailSplit[0];
    const day = nextAvailSplit[1];
    const month = nextAvailSplit[2];

    for (let i = 0; i < this.days.length; i++) {
      const d = this.days[i];
      if (d.dow === dow && d.day === day && d.month === month) {
        this.dateListItems.forEach(component => {
          if (component.index === i) component.selectDay(true);
        });

        // this.handleDateClicked(d, i);
      }

    }
  }

  deselectAllItems() {
    this.days?.forEach(d => {
      d.selected = false;
    });

  }

  selectDate(date = null, index: number) {
    if (date) this.selectedDate = date;
    this.selectedSlot = null;
    this.deselectAllItems();
  }

  selectSlot(slot) {
    this.selectedSlot = slot;
    this.deselectAllSlots();
    setTimeout(() => {
      this.confirm.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" });
    }, 100);

  }
  deselectAllSlots() {

    this.selectedDate.slots.forEach(s => {
      s.selected = false;
    });
  }

  back() {
    this.selectedDate = null;
    this.selectedSlot = null;
    this.goBack.emit(null)
  }

  // async getNewAppointments(prev: boolean, next: boolean): Promise<void> {
  //   const request: IPxChoiceGetAvailAppointmentReq = {
  //     siteCode: this.siteCode,
  //     ShowNext: next ? 1 : -1,
  //     ShowPrev: prev ? 1 : -1,
  //     StartDate: this.startDate,
  //     winWidth: this.winWidth,
  //     winHeight: this.winHeight,
  //   };

  //   const response: IPxChoiceGetAvailAppointmentRes = await this._appointmentService.getAvailableAppointments(
  //     request
  //   );

  //   this.startDate = response.StartDate;
  //   this.days = response.Days;
  //   this.selectedDate.nextAvail = (this.days[0] as IDay).nextAvail;
  // }


  // handleDateClicked(date: IDay, i: number) {
  //   this.showInitialCard = false;
  //   this.selectedDate = date;
  //   this.selected = i;
  //   this.applyStyle(i, date.status);

  //   if (date.status === 'S') {
  //     this.slotsForSelectedDay = this.days[i].slots;
  //   }

  // }
  // getBgColor(status: string): string {
  //   let bgColor: string = '';

  //   if (status === 'S') {
  //     bgColor = '#fff';
  //   } else {
  //     bgColor = 'darkGray';
  //   }

  //   return bgColor;
  // }

  // handleSlotClicked(slot: ISlot, i: number) {
  //   this.choosenDateAndTime.time = i;
  //   this.choosenDateAndTime.date = this.selected;
  //   this.selectionMade = true;
  //   this.selectedTime = slot.slotTime;
  //   this.selectedDuration = slot.duration.toString();
  //   this.selectedClinic = slot.clinicName;
  //   this.chosenAppointment =
  //     'Request appointment: ' +
  //     this.selectedDate.dow +
  //     ' ' +
  //     this.selectedDate.day +
  //     ' ' +
  //     this.selectedDate.month +
  //     ' at ' +
  //     this.selectedTime;
  // }

  // applyStyle(i: number, s: string) {
  //   let dateList = document.getElementById(
  //     'scrolling-wrapper'
  //   ) as HTMLSelectElement;
  //   let coll: HTMLCollection = dateList.children;

  //   for (let index = 0; index < coll.length; index++) {
  //     const element = coll[index].firstElementChild;

  //     if (i == index) {
  //       if (s === 'S') {
  //         element.className = 'card-body selected-date';
  //       } else {
  //         element.className = 'card-body selected-date-gray';
  //       }
  //     } else {
  //       element.className = '';
  //     }
  //   }
  // }

  // stepBack() {
  //   this.showInitialCard = true;
  //   this.chosenAppointment = '';

  //   let dateList = document.getElementById(
  //     'scrolling-wrapper'
  //   ) as HTMLSelectElement;
  //   let dates: HTMLCollection = dateList?.children;
  //   for (let i = 0; i < dates?.length; i++) {
  //     const outer = dates[i] as HTMLSelectElement;
  //     const inner = outer.firstElementChild as HTMLSelectElement;
  //     inner.className = '';
  //   }
  //   if (this.days?.length > 0) this.selectedDate.nextAvail = (this.days[0] as IDay)?.nextAvail;
  //   this.getAvailAppointments();
  //   this.stepper.previous();
  // }


}
