import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickAppointmentComponent } from './pick-appointment.component';

describe('PickAppointmentComponent', () => {
  let component: PickAppointmentComponent;
  let fixture: ComponentFixture<PickAppointmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickAppointmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickAppointmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
