import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickClinicComponent } from './pick-clinic.component';

describe('PickClinicComponent', () => {
  let component: PickClinicComponent;
  let fixture: ComponentFixture<PickClinicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickClinicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickClinicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
