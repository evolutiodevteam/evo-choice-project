import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { IPxChoicePractice } from 'src/app/models/IPxChoiceLogin.interface';
import { environment } from 'src/environments/environment';
// import Stepper from 'bs-stepper';
// import { faCheckCircle, faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { ISite } from 'src/app/models/ISite.interface';
// import { AppointmentService } from 'src/app/services/appointment.service';

@Component({
  selector: 'app-pick-clinic',
  templateUrl: './pick-clinic.component.html',
  styleUrls: ['./pick-clinic.component.scss'],
})
export class PickClinicComponent implements OnInit {
  @Output() siteCode: EventEmitter<ISite> = new EventEmitter<ISite>();
  sites: IPxChoicePractice[] = [];
  welcomeMessage: string = '';
  site: ISite;
  isExpanded: boolean = false;
  constructor() { }

  ngOnInit(): void {
    this.sites = environment.user.choicePractices || [];
    this.welcomeMessage = environment.user.welcomeMessage;
    //handle the collapse/expand event
    //first set all to !expanded
    this.collapseAllItems();


  }

  collapseAllItems() {
    this.sites.forEach(s => {
      s.expanded = false;
    });
  }


  choiceMade(site: IPxChoicePractice) {
    const siteCode = site.siteCode;
    const doesOnline: number = +site.doesOnline;
    const siteName = site.practiceName;
    const keyInfo = site.keyInfo.Strings;
    this.site = {
      siteCode,
      doesOnline,
      siteName,
      keyInfo
    }
    this.siteCode.emit(this.site);
    this.sites = [];
  }

}
