import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { IPxChoiceLoginRes } from '../models/IPxChoiceLogin.interface';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({
    dobDay: new FormControl('', [Validators.required, Validators.min(1), Validators.max(31)]),
    dobMonth: new FormControl('', [Validators.required, Validators.min(1), Validators.max(12)]),
    dobYear: new FormControl('', [Validators.required, Validators.minLength(2)]),
    ref: new FormControl('', [Validators.required]),
    mobile: new FormControl(''),
    email: new FormControl(''),
    noMobile: new FormControl(),
    noEmail: new FormControl(),
  });

  submitAttempted: boolean = false;
  isSubmitting: boolean;

  constructor(
    private _authService: LoginService,
    private _toastr: ToastrService,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this.loginForm.controls['noMobile'].valueChanges.subscribe(value => {
      value === true ? this.loginForm.controls['mobile'].disable() : this.loginForm.controls['mobile'].enable();
    });
    this.loginForm.controls['noEmail'].valueChanges.subscribe(value => {
      value === true ? this.loginForm.controls['email'].disable() : this.loginForm.controls['email'].enable();
    });
  }

  get emailOrPhoneProvided() {
    return this.loginForm.controls.mobile.value != '' || this.loginForm.controls.mobile.value != '';
  }

  get emailOrPhoneOverride() {
    const noEmail = this.loginForm.controls.noEmail.value === true;
    const noMobile = this.loginForm.controls.noMobile.value === true;

    return noEmail && noMobile;
  }

  async handleLogin(): Promise<void> {
    this.submitAttempted = true;
    if (this.loginForm.valid) {
      // a mobile or email is emailOrPhoneProvided
      // OR
      // both tickbox overrides have been checked
      if (this.emailOrPhoneProvided || this.emailOrPhoneOverride) {
        let day = this.autoCorrectDay(this.loginForm.get('dobDay').value);
        let month = this.autoCorrectMonth(this.loginForm.get('dobMonth').value);
        let year = this.autoCorrectYear(this.loginForm.get('dobYear').value);
        let date = this.getDate(day, month, year);
        if (date) {
          try {
            this.isSubmitting = true;

            await this._authService.loginAsPatient(date, this.loginForm.get('ref').value, this.loginForm.get('mobile')?.value, this.loginForm.get('email')?.value).then(res => {
              switch (res.successful) {
                case 1:
                  //handle success
                  this.handleStatusCode(res);
                  break;
                default:
                  //handle fail
                  this._toastr.error(res.errorMessage || 'Login error')
                  break;
              }
            })
          } catch (error) {
            console.log(error);
            this._toastr.error('Server error');
          } finally {
            this.isSubmitting = false;
          }

        }
      } else {
        this._toastr.info('Please provide an email address, or phone number.')
      }
    }
  }
  getDate(day: string, month: string, year: string) {
    if (day && month && year) return `${ day }/${ month }/${ year }`;
    else return undefined;
  }
  autoCorrectDay(dayString: string): string {

    let day: number = -1;//if no dayString has been entered this will catch the validation !(day > 0 && day < 32)
    if (dayString) day = +dayString;
    if (dayString && Number.isInteger(day)) {
      if (!(day > 0 && day < 32)) {
        // invalid
        this._toastr.info('Invalid day');
        return '';
      } else {
        // valid
        if (day < 10) return '0' + day.toString();
        else return day.toString();
      }
    } else {
      if (dayString) this._toastr.info('Invalid day');
      return '';
    }
  }
  autoCorrectMonth(mthString: string): string {
    let mth: number = -1; //if no mthString has been entered this will catch the validation !(mth > 0 && mth < 13
    if (mthString) mth = +mthString;

    if (mthString && Number.isInteger(mth)) {
      if (!(mth > 0 && mth < 13)) {
        this._toastr.info('Invalid month');
        return '';
      } else {
        if (mth < 10) return '0' + mth.toString();
        else return mth.toString();
      }
    } else {
      if (mthString) this._toastr.info('Invalid month');
      return '';
    }
  }

  autoCorrectYear(yrString: string): string {
    let yr: number = -1; //if no yrString has been entered this will catch the validation !(mth > 0 && mth < 13
    if (yrString) yr = +yrString;

    const thisYear = new Date().getFullYear();
    if (yrString && Number.isInteger(yr)) {
      switch (yr.toString().length) {
        case 2:
          yr = yr + 2000;
          if (yr > thisYear) yr = yr - 100;
          return yr.toString();
        case 4:
          if (yr && yr.toString().length === 4 && yr <= thisYear)
            return yr.toString();
        default:
          this._toastr.info('Invalid year');
          return '';
      }
    } else {
      if (yrString) this._toastr.info('Invalid year');
      return '';
    }
  }
  handleInvalidForm() {
    // touch all inputs to trigger invalid style class
    this.markFormGroupTouched(this.loginForm);
    //get and display error message
    // this._toastr.error(this.getErrorMessage(), 'Error');
  }

  // handleInvalidDate() {
  //   // as we wont know which part(s) of date are invalid, mark all as invalid
  //   this.loginForm.get('dobDate').setErrors({ 'invalid': true });
  //   this.loginForm.get('dobMonth').setErrors({ 'invalid': true });
  //   this.loginForm.get('dobYear').setErrors({ 'invalid': true });

  //   // display the correct error message
  //   if (!this.date.isValid()) this._toastr.error('Invalid date');
  //   else this._toastr.error("Date cannot be in future");

  // }

  handleStatusCode(res: IPxChoiceLoginRes) {
    switch (res.statusCode) {
      case 'OK':
        this._authService.setUser(res);
        this._router.navigate(['appointment']);
        break;
      case 'OKF':
        this._authService.setUser(res);
        this._authService.setStatus(res.statusCode);
        this._authService.setIsRecall(res.helpPhoneNumber);
        this._router.navigate(['appointment']);
        break;
      case 'BKF':
        this._authService.setErrorMessage(res.errorMessage);
        this._router.navigate(['patient-portal']);
        break;
      case 'BL':
        this._toastr.error(res.errorMessage, 'Login Error');
        break;
      case 'BK':
        this._authService.setErrorMessage(res.errorMessage);
        this._router.navigate(['patient-portal']);
        break;
      default:
        this._toastr.error(res.errorMessage || 'Unknown status code', 'Login error');
        break;
    }
  }

  // getErrorMessage() {
  //   // const validDOB = this.date?.isValid() && this.date?.isBefore(moment());
  //   // const validRef = this.loginForm.get('ref').valid;

  //   // if (validDOB && !validRef) return "Please enter the six digit reference number we sent you."
  //   // else if (!validDOB && validRef) return "Please enter your date of birth."
  //   // else if (!validDOB && !validRef) return "Please enter your date of birth and the six digit reference number we sent you."
  //   // else return '';
  // }

  // /**
  //  * scroll the element to the top of the screen. on mobile devices this will ensure the input is no hidden behind the keyboard
  //  * @param element the label of the selected input
  //  */
  // scrollTo(element: { id: string; }) {
  //   if (element.id === 'dateOfBirthRef') this.dateOfBirthRef.nativeElement.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });
  //   else if (element.id === 'monthOfBirthRef') this.monthOfBirthRef.nativeElement.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });
  //   else if (element.id === 'yearOfBirthRef') this.yearOfBirthRef.nativeElement.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });
  //   else if (element.id === 'ref') this.referenceNumberRef.nativeElement.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });
  //   else if (element.id === 'mobile') this.mobileNumberRef.nativeElement.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });
  //   else if (element.id === 'email') this.emailAddressRef.nativeElement.scrollIntoView({ behavior: "smooth", block: "start", inline: "nearest" });
  // }

  /**
 * Marks all controls in a form group as touched
 * @param formGroup - The form group to touch
 *
 * https://stackoverflow.com/a/44150793
 */
  private markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();
      if (control.controls) this.markFormGroupTouched(control);
    });
  }
}
