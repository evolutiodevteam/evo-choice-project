import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { AppointmentComponent } from './appointment/appointment.component';
import { HttpClientModule } from '@angular/common/http';
import { AppointmentService } from './services/appointment.service';
import { LoginService } from './services/login.service';
import { AuthGuardService } from './services/auth-guard.service';
import { PickClinicComponent } from './appointment/pick-clinic/pick-clinic.component';
import { PickAppointmentComponent } from './appointment/pick-appointment/pick-appointment.component';
import { ConfirmComponent } from './appointment/confirm/confirm.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgSelectModule } from '@ng-select/ng-select';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SharedModule } from './shared/shared.module';
import { AppointmentCompleteComponent } from './appointment-complete/appointment-complete.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    AppointmentComponent,
    PickClinicComponent,
    PickAppointmentComponent,
    ConfirmComponent,
    AppointmentCompleteComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgSelectModule,
    FormsModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    SharedModule,
  ],
  providers: [AppointmentService, LoginService, AuthGuardService],
  bootstrap: [AppComponent],
})
export class AppModule {}
