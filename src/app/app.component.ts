import { Component, OnDestroy } from '@angular/core';
import { LoginService } from './services/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy {
  title='evo-choice-app';
  constructor(private _authService: LoginService) { }
  
  ngOnDestroy() {
    this._authService.clearUser();
  }
}
