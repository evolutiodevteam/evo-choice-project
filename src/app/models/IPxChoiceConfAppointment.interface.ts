export interface IPxChoiceConfAppointmentReq {
  siteCode: string;
  month?: string;
  day?: string;
  slotTime?: string;
  duration?: string;
  refNum?:string;
}

export interface IPxChoiceConfAppointmentRes {
  success: number;
  messageText: string;
  month?: string;
  day?: string;
  slotTime?: string;
}

export interface IConfirmationObject {
  siteCode: string;
  messageText: string;
  month?: string;
  day?: string;
  slotTime?: string;
  pbId?: string;
  keyInfo?: string[];
}
