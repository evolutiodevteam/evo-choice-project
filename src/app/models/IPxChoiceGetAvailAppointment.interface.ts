export interface IPxChoiceGetAvailAppointmentReq {
  winWidth: number;
  winHeight: number;
  ShowNext: number;
  ShowPrev: number;
  StartDate: number;
  siteCode: string;
}

export interface IPxChoiceGetAvailAppointmentRes {
  Successful: number;
  ErrorMsg: string;
  StartDate: number;
  Days: IDay[];
  Message: string;
}
export interface IDay {
  month: string;
  dow: string;
  day: string;
  status: string;
  nextAvail: string;
  slots: ISlot[];
  //client side only
  selected?:boolean;
}
export interface ISlot {
  slotTime: string;
  duration: number;
  clinicName: string;
   //client side only
   selected?:boolean;
}

export interface IChoosenDateAndTime{
  date: number;
  time: number;
}

export interface IPxAvailableAppointments{
  appointments
}

export interface IGetPracticesRes {
  choicePractices: any[];
  patientName: string;
  statusCode: string;
  successful: number;
  welcomeMessage: string;
}