export interface IPxChoiceReqAppointmentReq {
  siteCode: string;
  month: string;
  day: string;
  slotTime: string;
  duration: string;
  clinicName: string;
}

export interface IPxChoiceReqAppointmentRes {
  success: number;
  messageText: string;
  month: string;
  dow: string;
  day: string;
  slotTime: string;
  pbId: string;
}
