export interface ISite {
  siteCode: string;
  doesOnline: number;
  siteName: string;
  keyInfo?: string[];
}
