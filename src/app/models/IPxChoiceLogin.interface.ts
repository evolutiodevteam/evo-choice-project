export interface IPxChoiceLoginReq {
  pxDOB: string;
  pxPin: string;
  pxMobile: string;
}

export interface IPxChoiceLoginRes {
  successful: number;
  jwt: string;
  patientName: string;
  welcomeMessage: string;
  choicePractices: IPxChoicePractice[];
  errorMessage?: string;
  userId: string;
  statusCode: string;
  helpPhoneNumber:string;
}

export interface IPxChoicePractice {
  siteCode?: string;
  practiceName?: string;
  practiceAddress?: string;
  doesOnline?: number;
  nextAvailBooking?: string; //new
  distance?: string;
  keyInfo?: IInfo;
  //following is for client side only
  expanded?:boolean;
}

export interface IInfo{
  Strings: string[];
}