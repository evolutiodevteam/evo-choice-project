import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  message: string = '';
  constructor(private _router: Router) {}

  ngOnInit(): void {
    if (environment.errorMessage) this.message = environment.errorMessage;
    else this._router.navigate(['auth']);
  }

  exit(): void {
    environment.errorMessage = '';
    this._router.navigate(['auth']);
  }
}
