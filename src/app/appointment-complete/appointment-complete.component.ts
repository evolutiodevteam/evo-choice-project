import { Component, Input, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Component({
  selector: 'app-appointment-complete',
  templateUrl: './appointment-complete.component.html',
  styleUrls: ['./appointment-complete.component.scss'],
})
export class AppointmentCompleteComponent implements OnInit {
  @Input() noDate: boolean = false;
  message: string;

  constructor(private _router: Router) {}

  ngOnInit(): void {
    this.message = this.noDate ? environment.errorMessage:environment.user.welcomeMessage;
  }
}
