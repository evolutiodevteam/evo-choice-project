import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import {
  NegateAuthGuardService,
  AuthGuardService,
  DoneAuthGuard,
} from './services/auth-guard.service';
import { AppointmentComponent } from './appointment/appointment.component';
import { AppointmentCompleteComponent } from './appointment-complete/appointment-complete.component';
import { PatientPortalModule } from './patient-portal/patient-portal.module';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'auth' },
  {
    path: 'auth',
    component: AuthComponent,
    canActivate: [NegateAuthGuardService],
  },
  {
    path: 'appointment',
    component: AppointmentComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: 'appointment-complete',
    component: AppointmentCompleteComponent,
    canActivate: [DoneAuthGuard],
  },
  {
    path: 'patient-portal',
    loadChildren: () => PatientPortalModule ,
    // canActivate: [AuthGuardService],
  },
  {
    path: '**',
    redirectTo: 'auth',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
