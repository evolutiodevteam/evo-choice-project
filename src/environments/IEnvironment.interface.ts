import { IPxChoicePractice } from 'src/app/models/IPxChoiceLogin.interface';

export interface IEnvironment {
    refNum:string;
    production: boolean
    evonnectSSSUrl: string
    user: {
        jwt: string,
        patientName?: string,
        welcomeMessage?: string,
        choicePractices?: IPxChoicePractice[]
        userId?: string
    },
    statusCode: string;
    errorMessage: string
    isRecall:boolean;
    helpPhoneNumber:string;
}