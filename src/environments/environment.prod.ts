import { IEnvironment } from './IEnvironment.interface';

export const environment: IEnvironment = {
  refNum:'',

  production: true,
  evonnectSSSUrl: "https://sss.evonnect.com/SSS",
  user: {
    jwt: ''
  },
  statusCode:'',
  errorMessage:'',
  isRecall:false,
  helpPhoneNumber:'',
  
};
